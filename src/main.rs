use chrono::prelude::*;
use rusqlite::params;
use rusqlite::Connection;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

#[macro_use]
extern crate serde_derive;

struct Args {
    indir: Result<String, pico_args::Error>,
    outdir: Result<String, pico_args::Error>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Book {
    id: i32,
    title: String,
    rating: Option<i32>,
    date: Option<String>,
    authors: String,
    tags: String,
    identifiers: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Author {
    name: String,
    book_id: i32,
}

#[derive(Debug, Serialize, Deserialize)]
struct Tag {
    name: String,
    book_id: i32,
}

#[derive(Debug, Serialize, Deserialize)]
struct Identifier {
    book_id: i32,
    ident: String,
    value: String,
}

fn main() {
    let mut args = pico_args::Arguments::from_env();
    let args = Args {
        indir: args.value_from_str("--indir"),
        outdir: args.value_from_str("--outdir"),
    };

    if let Ok(indir) = args.indir {
        if let Some(conn) = open_connection(&indir) {
            let mut books = get_books(&conn);
            let authors = get_authors(&conn);
            let tags = get_tags(&conn);
            let identifiers = get_identifiers(&conn);

            for book in &mut books {
                book.authors = get_book_authors(book.id, &authors);
                book.tags = get_book_tags(book.id, &tags);
                book.identifiers = get_book_identifiers(book.id, &identifiers);
            }

            if let Ok(outdir) = args.outdir {
                let path = PathBuf::from(outdir);
                if let Ok(books_json) = serde_json::to_string(&books) {
                    let mut wbooks_md = File::create(path.join("books.json")).unwrap();
                    wbooks_md.write_all(books_json.as_bytes()).unwrap();
                }
            }
        }
    }
}

fn open_connection(indir: &str) -> Option<rusqlite::Connection> {
    if let Ok(conn) = Connection::open(get_db_file(indir)) {
        pre_query(&conn);

        return Some(conn);
    }

    None
}

fn get_books(conn: &Connection) -> Vec<Book> {
    let sql = r#"SELECT b.id, b.title, r.rating, STRFTIME("%Y", cc2.value) AS year FROM books b
LEFT JOIN books_ratings_link brl ON b.id = brl.book
LEFT JOIN ratings r ON brl.rating = r.id
LEFT JOIN custom_column_1 cc ON b.id = cc.book
LEFT JOIN custom_column_2 cc2 ON b.id = cc2.book
WHERE cc.value = 1 AND r.rating > 8
ORDER BY r.rating DESC, b.title ASC;"#;

    let mut stmt = conn.prepare(sql).unwrap();
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Book {
                id: row.get(0).unwrap(),
                title: row.get(1).unwrap(),
                rating: try_rating(row.get(2)),
                date: try_date(row.get(3)),
                authors: String::new(),
                tags: String::new(),
                identifiers: String::new(),
            })
        })
        .unwrap();

    rows.map(|x| x.unwrap()).collect()
}

fn try_rating(rating: Result<i32, rusqlite::Error>) -> Option<i32> {
    if let Ok(r) = rating {
        return Some(r);
    }

    None
}

fn try_date(rating: Result<String, rusqlite::Error>) -> Option<String> {
    if let Ok(r) = rating {
        return Some(r);
    }

    None
}

fn get_authors(conn: &Connection) -> Vec<Author> {
    let sql = r#"SELECT a.name, bal.book FROM authors a
JOIN books_authors_link bal ON bal.author = a.id
ORDER BY a.name ASC;"#;

    let mut stmt = conn.prepare(sql).unwrap();
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Author {
                name: row.get(0).unwrap(),
                book_id: row.get(1).unwrap(),
            })
        })
        .unwrap();

    rows.map(|x| x.unwrap()).collect()
}

fn get_tags(conn: &Connection) -> Vec<Tag> {
    let sql = r#"SELECT t.name, btl.book FROM tags t
JOIN books_tags_link btl ON t.id = btl.tag
WHERE t.name NOT IN ("chriswillx", "printed", "General", "wish", "elsevier")
ORDER BY t.name ASC;"#;

    let mut stmt = conn.prepare(sql).unwrap();
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Tag {
                name: row.get(0).unwrap(),
                book_id: row.get(1).unwrap(),
            })
        })
        .unwrap();

    rows.map(|x| x.unwrap()).collect()
}

fn get_identifiers(conn: &Connection) -> Vec<Identifier> {
    let sql = r#"SELECT i.book, i."type", i.val FROM identifiers i
WHERE i."type"IN ("isbn", "isbn-13", "isbn10", "isbn13")
ORDER BY i.book ASC, i.val ASC;"#;

    let mut stmt = conn.prepare(sql).unwrap();
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Identifier {
                book_id: row.get(0).unwrap(),
                ident: row.get(1).unwrap(),
                value: row.get(2).unwrap(),
            })
        })
        .unwrap();

    rows.map(|x| x.unwrap()).collect()
}

fn get_book_authors(book_id: i32, authors: &[Author]) -> String {
    authors
        .iter()
        .filter(|author| author.book_id == book_id)
        .map(|author| author.name.clone())
        .collect::<Vec<String>>()
        .join(", ")
}

fn get_book_tags(book_id: i32, tags: &[Tag]) -> String {
    tags.iter()
        .filter(|tag| tag.book_id == book_id)
        .map(|tag| tag.name.clone())
        .collect::<Vec<String>>()
        .join(", ")
}

fn get_book_identifiers(book_id: i32, identifiers: &[Identifier]) -> String {
    identifiers
        .iter()
        .filter(|id| id.book_id == book_id)
        .map(|id| format!("{}: {}", id.ident, id.value))
        .collect::<Vec<String>>()
        .join(", ")
}

fn get_db_file(indir: &str) -> PathBuf {
    let root = Path::new(indir);
    root.join("metadata.db")
}

fn pre_query(conn: &Connection) {
    let sql = r#"
        pragma journal_mode = WAL;
        pragma synchronous = normal;
        pragma temp_store = memory;
        pragma mmap_size = 2147483648;
        "#;

    match conn.execute_batch(sql) {
        Ok(_) => {}
        Err(e) => {
            println!("pre_query {} - {}", Utc::now(), e);
        }
    }
}
